package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"

	"github.com/graphql-go/graphql"
)

// CreateBlock creates a new block and returns it.
func CreateBlock() *graphql.Field {
	return &graphql.Field{
		Type: types.BlockType,
		Args: graphql.FieldConfigArgument{
			"title": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"body": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"index": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.Int),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create block\n")
			log.WithFields(log.Fields{
				"title": params.Args["title"].(string),
				"body":  params.Args["body"].(string),
				"index":  params.Args["index"].(string),
			}).Debug("__ create block\n")

			block := &types.Block{
				Title: params.Args["title"].(string),
				Body:  params.Args["body"].(string),
				Index:  params.Args["index"].(int),
			}

			repository.Create(block)

			return block, nil
		},
	}
}
