package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateListUsers creates a new user role and returns it.
func CreateListUsers() *graphql.Field {
	return &graphql.Field{
		Type: types.ListUsersType,
		Args: graphql.FieldConfigArgument{
			"id_user": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"id_role": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create user role\n")
			log.WithFields(log.Fields{
				"id_user": params.Args["id_user"].(string),
				"id_role": params.Args["id_role"].(string),
			}).Debug("__ create user role\n")

			idUser, _ := uuid.FromString(params.Args["id_user"].(string))
			idRole, _ := uuid.FromString(params.Args["id_role"].(string))

			userRole := &types.ListUsers{
				ID_user: idUser,
				ID_role: idRole,
			}

			repository.Create(userRole)

			return userRole, nil
		},
	}
}
