package mutations

import (
	"server/repository"
	"server/types"
	"server/utils"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateReaction creates a new reaction and returns it.
func CreateReaction() *graphql.Field {
	return &graphql.Field{
		Type: types.ReactionType,
		Description: "return new reaction and create relationship with user",
		Args: graphql.FieldConfigArgument{
			"title": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idUser" : &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idProject" : &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create reaction\n")
			log.WithFields(log.Fields{
				"title": params.Args["title"].(string),
				"idUser":    params.Args["idUser"].(string),
				"idProject":   params.Args["idProject"].(string),
			}).Debug("__ create reaction\n")

			reaction := &types.Reaction{
				Title: params.Args["title"].(string),
				CreatedAt: utils.GetDate(),
			}

			repository.Create(reaction)

			// Create relationship between new reaction and user

			idUser, _ := uuid.FromString(params.Args["idUser"].(string))
			idProject, _ := uuid.FromString(params.Args["idProject"].(string))

			listsReactions := &types.ListsReactions{
				ID_reaction: reaction.ID,
				ID_user:	idUser,
				ID_project:	idProject,
			}

			log.WithFields(log.Fields{
				"idReaction": reaction.ID,
				"title": reaction.Title,
			}).Debug("__ create reaction __ before create list reaction\n")

			repository.Create(listsReactions)

			return reaction, nil
		},
	}
}
