package mutations

import (
	"server/repository"
	"server/types"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateListsBlock creates a new lists blocks and returns it.
func CreateListsBlock() *graphql.Field {
	return &graphql.Field{
		Type: types.ListsBlocksType,
		Args: graphql.FieldConfigArgument{
			"id_project": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"id_block": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create list block\n")
			log.WithFields(log.Fields{
				"id_block":   params.Args["id_block"].(string),
				"id_project": params.Args["id_project"].(string),
			}).Debug("__ create list block\n")

			idBlock, _ := uuid.FromString(params.Args["id_block"].(string))
			idProject, _ := uuid.FromString(params.Args["id_project"].(string))
		
			listsBlocks := &types.ListsBlocks{
				ID_block:   idBlock,
				ID_project: idProject,
			}

			repository.Create(listsBlocks)

			return listsBlocks, nil
		},
	}
}
