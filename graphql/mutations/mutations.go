package mutations

import (
	"github.com/graphql-go/graphql"
)

// GetRootFields returns all the available mutations.
func GetRootFields() graphql.Fields {
	return graphql.Fields{
		"CreateUser":     CreateUser(),
		"CreateRole":     CreateRole(),
		"UserRole": CreateListUsers(),
		"CreateOpinion":  CreateOpinion(),
		// "ListsOpinions":  CreateListsOpinions(),
		"CreateReaction": CreateReaction(),
		// "ListsReactions": CreateListsReactions(),
		"CreateProject":       CreateProject(),
		// "ListsProjects": CreateListsProjects(),
		"CreateBlock":         CreateBlock(),
		// "ListsBlock":    CreateListsBlocks(),
	}
}
