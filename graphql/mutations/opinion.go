package mutations

import (
	"server/repository"
	"server/types"
	"server/utils"

	log "github.com/sirupsen/logrus"
	"github.com/satori/go.uuid"

	"github.com/graphql-go/graphql"
)

// CreateOpinion creates a new opinion and returns it.
func CreateOpinion() *graphql.Field {
	return &graphql.Field{
		Type: types.OpinionType,
		Description: "return new opinion and create relationship with user",
		Args: graphql.FieldConfigArgument{
			"title": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"description": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idUser" : &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			"idProject" : &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[mutation] create opinion\n")
			log.WithFields(log.Fields{
				"title":       params.Args["title"].(string),
				"description": params.Args["description"].(string),
				"idUser":    params.Args["idUser"].(string),
				"idProject":   params.Args["idProject"].(string),
			}).Debug("__ create opinion __ input param\n")

			opinion := &types.Opinion{
				Title:       params.Args["title"].(string),
				Description: params.Args["description"].(string),
				CreatedAt: utils.GetDate(),
			}

			repository.Create(opinion)

			// Create relationship between new opinion and user

			idUser, _ := uuid.FromString(params.Args["idUser"].(string))
			idProject, _ := uuid.FromString(params.Args["idProject"].(string))

			log.WithFields(log.Fields{
				"idOpinion":   opinion.ID,
			}).Debug("__ create opinion __ before create list opinion\n")

			listsOpinions := &types.ListsOpinions{
				ID_opinion:	opinion.ID,
				ID_user:	idUser,
				ID_project:	idProject,
			}

			repository.Create(listsOpinions)

			return opinion, nil
		},
	}
}
