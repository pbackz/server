package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

// FindRole returns role from id.
func FindRole() *graphql.Field {
	return &graphql.Field{
		Type:        types.RoleType,
		Description: "Return Role from ID",
		Args: graphql.FieldConfigArgument{
			"id": &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Role \n")
			log.WithFields(log.Fields{
				"id": params.Args["id"].(string),
			}).Debug("__ Query Role\n")

			var role types.Role

			id, _ := uuid.FromString(params.Args["id"].(string))

			repository.Find(&role, id)

			return role, nil
		},
	}
}

// FindListRoles
func FindListRoles() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(types.RoleType),
		Args: graphql.FieldConfigArgument{
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: 0,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] Query ListRoles\n")
			log.WithFields(log.Fields{
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListRoles\n")

			var roles []types.Role

			repository.FindAll(&roles, params.Args["offset"].(int), params.Args["limit"].(int))

			return roles, nil
		},
	}
}
