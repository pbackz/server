package queries

import (
	"github.com/graphql-go/graphql"
)

// GetRootFields returns all the available queries.
func GetRootFields() graphql.Fields {
	return graphql.Fields{
		"Login":         Login(),
		"Refresh":       Refresh(),
		"User":          FindUser(),
		"Project":       FindProject(),
		"Opinion":       FindOpinion(),
		"Reaction":      FindReaction(),
		"Role":          FindRole(),
		"ListUser":      FindListUser(),
		"ListProjects":  FindListProject(),
		"ListOpinions":  FindListOpinion(),
		"ListReactions": FindListReaction(),
		"ListBlocks":    FindListBlocks(),
		"provideUser":   GetProvideUserQuery(),
	}
}
