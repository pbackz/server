package queries

import (
	log "github.com/sirupsen/logrus"

	"server/repository"
	"server/types"

	"github.com/satori/go.uuid"
	"github.com/graphql-go/graphql"
)

// FindListOpinion return list of opinion from a project
func FindListOpinion() *graphql.Field {
	return &graphql.Field{
		Type:        graphql.NewList(types.OpinionType),
		Description: "return list of opinion",
		Args: graphql.FieldConfigArgument{
			"id_project": &graphql.ArgumentConfig{
				Type:         graphql.NewNonNull(graphql.String),
			},
			"offset": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
			"limit": &graphql.ArgumentConfig{
				Type:         graphql.Int,
				DefaultValue: -1,
			},
		},
		Resolve: func(params graphql.ResolveParams) (interface{}, error) {
			log.Info("[query] FindListOpinionBy\n")
			log.WithFields(log.Fields{
				"id":     params.Args["id_project"].(string),
				"offset": params.Args["offset"].(int),
				"limit":  params.Args["limit"].(int),
			}).Debug("__ Query ListsOpinionBy\n")

			var ret []types.ListsOpinions
			var opinions []types.Opinion

			id, _ := uuid.FromString(params.Args["id_project"].(string))

			repository.FindListBy(&ret, "id_project", id, params.Args["offset"].(int), params.Args["limit"].(int))

			listIndex := getIdOpinion(ret)

			repository.FindByList(&opinions, listIndex)

			return opinions, nil
		},
	}
}

func getIdOpinion(source []types.ListsOpinions) ([]string) {
	new_tab := make([]string, len(source))

	for i, e := range source {
		new_tab[i] = e.ID_opinion.String()
	}

	return new_tab
}