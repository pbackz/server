package graphql

import (
	"server/graphql/mutations"
	"server/graphql/queries"

	"github.com/graphql-go/graphql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

// init shema config for graphQl
func InitGraphQL() graphql.Schema {
	schemaConfig := graphql.SchemaConfig{
		Query: graphql.NewObject(graphql.ObjectConfig{
			Name:   "Query",
			Description: "List of queries, to validate and execute, retrieve data",
			Fields: queries.GetRootFields(),
		}),
		Mutation: graphql.NewObject(graphql.ObjectConfig{
			Name:   "Mutation",
			Description: "List of mutation, to validate and store data",
			Fields: mutations.GetRootFields(),
		}),
	}

	schema, err := graphql.NewSchema(schemaConfig)

	if err != nil {
		log.Fatal("Failed to create new schema, error: %v", err)
	}

	return schema
}
