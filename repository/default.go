package repository

import (
	"server/utils"
	"github.com/satori/go.uuid"
)

// Find with an id in database
func Find(table interface{}, id uuid.UUID) {
	utils.DB.Find(table, "id = ?", id)
}

// Find with an id in database
func FindByList(table interface{}, id []string) {
	utils.DB.Find(table, id)
}

// Create in database
func Create(table interface{}) {
	utils.DB.Create(table)
}

// Find all in database
func FindAll(table interface{}, limit int, offset int) {
	utils.DB.Limit(limit).Offset(offset).Find(table)
}

// Find by param
func FindListBy(table interface{}, by string, id uuid.UUID, offset int, limit int) {
	utils.DB.Offset(offset).Limit(limit).Find(table, by+" = ?", id)
}

// Find by param
func FindListSelectedBy(table interface{},selected string, by string, id uuid.UUID, offset int, limit int) {
	utils.DB.Select(selected).Offset(offset).Limit(limit).Find(table, by+" = ?", id)
}

func FindUser(table interface{}, username string) bool {
	result := utils.DB.Find(table, "username = ?", username)
	return result.RowsAffected == 0
}