package services

import (
	"encoding/json"
	"fmt"
	"server/types"
	"server/utils"
)

// Inintialize fixture from json
func InitFixture() {
	var err error
	var byteValue []byte
	fmt.Println("Init fixture ...")

	fmt.Println("Create Users")
	var user types.User
	byteValue, err = utils.ReadJSON("/json/user.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &user)
	utils.DB.Create(&user)
	fmt.Println("Create Opinions")

	var opinions []types.Opinion
	byteValue, err = utils.ReadJSON("/json/listopinions.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &opinions)
	for _, opinion := range opinions {
		utils.DB.Create(&opinion)
	}

	fmt.Println("Create Reactions")

	var reactions []types.Reaction
	byteValue, err = utils.ReadJSON("/json/listreactions.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &reactions)
	for _, reaction := range reactions {
		utils.DB.Create(&reaction)
	}

	fmt.Println("Create Projects")

	var projects []types.Project
	byteValue, err = utils.ReadJSON("/json/listprojects.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &projects)
	for _, project := range projects {
		utils.DB.Create(&project)
	}

	fmt.Println("Create Roles")

	var roles []types.Role
	byteValue, err = utils.ReadJSON("/json/listroles.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &roles)

	for _, role := range roles {
		utils.DB.Create(&role)
	}

	fmt.Println("Create Block")

	var blocks []types.Block
	byteValue, err = utils.ReadJSON("/json/listblock.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &blocks)

	for _, block := range blocks {
		utils.DB.Create(&block)
	}

	fmt.Println("Create list opinions")

	var listOpinions []types.ListsOpinions
	byteValue, err = utils.ReadJSON("/json/listOpinionsRelation.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &listOpinions)

	for _, block := range listOpinions {
		utils.DB.Create(&block)
	}

	fmt.Println("Create list reactions")

	var listReaction []types.ListsReactions
	byteValue, err = utils.ReadJSON("/json/listReactionRelation.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &listReaction)

	for _, block := range listReaction {
		utils.DB.Create(&block)
	}

	fmt.Println("Create list user role")

	var userRole []types.ListUsers
	byteValue, err = utils.ReadJSON("/json/ListUsers.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &userRole)

	for _, block := range userRole {
		utils.DB.Create(&block)
	}

	fmt.Println("Create list block project")

	var blockProject []types.ListsBlocks
	byteValue, err = utils.ReadJSON("/json/listBlockProject.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &blockProject)

	for _, block := range blockProject {
		utils.DB.Create(&block)
	}

	fmt.Println("Create list project user")

	var projectUser []types.ListsProject
	byteValue, err = utils.ReadJSON("/json/listProjectUser.json")
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(byteValue, &projectUser)

	for _, block := range projectUser {
		utils.DB.Create(&block)
	}

	fmt.Println("End fixture ...")

}
