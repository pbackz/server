package services

import (
	"server/types"

	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"

	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

func ConnectWith42(params graphql.ResolveParams, user *types.User) {

	response, err := http.PostForm("https://api.intra.42.fr/oauth/token", url.Values{
		"grant_type":    {"authorization_code"},
		"client_id":     {"9bebbad07225b94bd4346d1ad0a2e00804db403b3d64ace1933a05c38904b5d8"},
		"client_secret": {"6fac91978092c67913bb7d0d35149c0ccbd495cfe6147fec546c18db78206ea2"},
		"code":          {params.Args["code"].(string)},
		"redirect_uri":  {"http://localhost:3000"},
		"state":         {params.Args["state"].(string)}})

	if err != nil {
		println(err)
	}
	type Response struct {
		Access_token string
	}
	res1 := new(Response)

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		println(err)
	}
	json.Unmarshal(body, &res1)

	var access_token = string(res1.Access_token)
	time.Sleep(1500 * time.Millisecond)
	userResponse, err := http.Get("https://api.intra.42.fr/v2/me?access_token=" + string(access_token))
	if err != nil {
		println(err)
	}
	defer userResponse.Body.Close()
	body_, err := ioutil.ReadAll(userResponse.Body)
	if err != nil {
		println(err)
	}

	type ResponseUser struct {
		Id         uuid.UUID
		Email      string
		First_name string
		Last_name  string
	}
	responseUserData := new(ResponseUser)
	json.Unmarshal(body_, &responseUserData)

	user.ID = responseUserData.Id
	user.Firstname = responseUserData.First_name
	user.Lastname = responseUserData.Last_name
	user.Email = responseUserData.Email
	user.Password = "42"
	user.Job = "student"
	user.Bio = "42"
	user.Age = "none"
	user.CreatedAt = "none"
}
