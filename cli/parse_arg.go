package cli

import (
	"server/services"
	"server/utils"
)

// Find and return position of an element
func findInTab(list []string, a string) int {
	for i, b := range list {
		if b == a {
			return i
		}
	}
	return -1
}

//  Parse input param
func ParseArguments(tab []string) bool {
	var tmp = 0

	tmp = findInTab(tab, "--fixtures")
	if tmp != -1 {
		services.InitFixture()
		return false
	}

	tmp = findInTab(tab, "--log_level")
	if tmp != -1 && len(tab) > tmp+1 {
		utils.InitLogger(tab[tmp+1])
	} else {
		utils.InitLogger("info")
	}
	return true
}
