package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
)

// Lists Opinions type definition.
type ListsProject struct {
	ID_project	uuid.UUID	`gorm:"type:uuid" db:"id_project" json:"id_project"`
	ID_user	uuid.UUID	`gorm:"type:uuid" db:"ID_user" json:"ID_user"`
}

// ListsProjectType is the GraphQL schema for the project user relation type.
var ListsProjectType = graphql.NewObject(graphql.ObjectConfig{
	Name: "ListsProject",
	Fields: graphql.Fields{
		"id_project": &graphql.Field{Type: graphql.String},
		"ID_user":  &graphql.Field{Type: graphql.String},
	},
})
