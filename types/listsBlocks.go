package types

import (
	"github.com/graphql-go/graphql"
	"github.com/satori/go.uuid"
)

// Lists Opinions type definition.
type ListsBlocks struct {
	ID_project uuid.UUID    `gorm:"type:uuid" db:"id_project" json:"id_project"`
	ID_block   uuid.UUID    `gorm:"type:uuid" db:"id_block" json:"id_block"`
	Title      string       `db:"title" json:"title"`
}

// listsBlockType is the GraphQL schema for the project user relation type.
var ListsBlocksType = graphql.NewObject(graphql.ObjectConfig{
	Name: "listsBlock",
	Fields: graphql.Fields{
		"id_project": &graphql.Field{Type: graphql.String},
		"id_block":   &graphql.Field{Type: graphql.String},
	},
})
