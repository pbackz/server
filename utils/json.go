package utils

import (
	"fmt"
	"io/ioutil"

	log "github.com/sirupsen/logrus"

	"os"
)

// ReadJSON returns the queries available against user type.
func ReadJSON(file string) ([]byte, error) {
	path, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	// Open our jsonFile
	jsonFile, err := os.Open(path + file)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	// read our opened jsonFile as a byte array.
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return byteValue, err
}
